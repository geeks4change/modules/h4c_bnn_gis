<?php

namespace Drupal\h4c_bnn_gis;

class BnnGisStreetDataMatcher {

  protected BnnGisStreetData $streetData;

  public function __construct(BnnGisStreetData $streetData) {
    $this->streetData = $streetData;
  }

  public function match(string $rawAddress): ?string {
    $isRelevant = preg_match('#(?:^|[\s,;.])bonn(?:$|[\s,;./\\-])#ui', $rawAddress);
    if (!$isRelevant) {
      return NULL;
    }
    foreach ($this->streetData->getStreets() as $street) {
      $streetAndNumberRegex = $this->getRegex($street);
      if (preg_match($streetAndNumberRegex, $rawAddress, $matches)) {
        $rawNumber = $matches['number'];
        if ($id = $this->streetData->getId($street, $rawNumber)) {
          return $id;
        }
      }
    }
    return NULL;
  }

  public function getRegex(string $street): string {
    // Regular expression massaging...
    // Match Adolf-Adler|Adolf Adler
    $streetRegex = $street;
    // Match Badstraße|Badstr.
    $streetRegex = preg_replace('#(str)aße$#ui', '\1(?:aße|[.])?', $streetRegex);
    // Match Bonzenplatz|Bonzenpl.
    $streetRegex = preg_replace('#(pl)atz$#ui', '\1(?:atz|[.])?', $streetRegex);
    // Finally, umlaut.
    $streetRegex = preg_replace('#(ä|ae)#ui', '(?:ä|ae)', $streetRegex);
    $streetRegex = preg_replace('#(ö|oe)#ui', '(?:ö|oe)', $streetRegex);
    $streetRegex = preg_replace('#(ü|ue)#ui', '(?:ü|ue)', $streetRegex);
    $streetRegex = preg_replace('#(ß|ss)#ui', '(?:ß|ss)', $streetRegex);
    // Comma, space or nothing.
    $streetRegex = preg_replace('#( |-)#ui', '[ -]?', $streetRegex);
    // Match number after street.
    $streetAndNumberRegex = "#{$streetRegex} *(?<number>[0-9]+[a-z]?(?:-[0-9]+)?)#ui";
    return $streetAndNumberRegex;
  }

}
