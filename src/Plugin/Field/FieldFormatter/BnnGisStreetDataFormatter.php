<?php

namespace Drupal\h4c_bnn_gis\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\h4c_bnn_gis\BnnGisStreetDataMatcher;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'h4c_bnn_gis__street' formatter.
 *
 * @FieldFormatter(
 *   id = "h4c_bnn_gis__street",
 *   label = @Translation("Bonn Street ID"),
 *   field_types = {
 *     "string",
 *   },
 * )
 */
class BnnGisStreetDataFormatter extends FormatterBase {

  protected BnnGisStreetDataMatcher $streetDataMatcher;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
    $instance->streetDataMatcher = $container->get('h4c_bnn_gis.matcher');
    return $instance;
  }


  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $i => $item) {
      assert($item instanceof FieldItemInterface);
      $address = $item->getString();
      $addressId = $this->streetDataMatcher->match($address);
      $elements[$i] = [
        '#plain_text' => $addressId,
        '#cache' => ['tags' => ['h4c_bnn_gis']],
      ];
    }
    return $elements;
  }

}