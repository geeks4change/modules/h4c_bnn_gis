<?php

namespace Drupal\h4c_bnn_gis;

final class BnnGisStreetData {

  /**
   * Array: Street => FullNumber => ID
   *
   * @var array<array<string, string>, string>
   */
  protected array $data;

  protected function __construct(array $data) {
    $this->data = $data;
  }

  public static function fromData(array $data) {
    return new static($data);
  }

  public function toData(): array {
    return $this->data;
  }

  public function getStreets(): array {
    return array_keys($this->data);
  }

  public function getNumbers(string $street): ?array {
    $idsByNumber = $this->data[$street] ?? NULL;
    return isset($idsByNumber) ? array_keys($idsByNumber) : NULL;
  }

  public function getIdByNumberMap(string $street): ?array {
    return $this->data[$street] ?? NULL;
  }

  public function getStreetAndNumberMapByIdMap(): ?array {
    $result = [];
    foreach ($this->data as $street => $streetData) {
      foreach ($streetData as $number => $id) {
        $result[$id] = "$street $number";
      }
    }
    return $result;
  }

  public function getId(string $street, string $number): ?string {
    return $this->data[$street][$number] ?? NULL;
  }

}
