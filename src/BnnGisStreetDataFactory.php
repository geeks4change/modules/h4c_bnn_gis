<?php

namespace Drupal\h4c_bnn_gis;

use Drupal\Core\Cache\CacheBackendInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Header;
use League\Csv\Reader;

class BnnGisStreetDataFactory {

  protected CacheBackendInterface $cacheBackend;

  public function __construct(CacheBackendInterface $cacheBackend) {
    $this->cacheBackend = $cacheBackend;
  }

  public function get(): BnnGisStreetData {
    // Give back a service even if no data, but only for this request.
    return BnnGisStreetData::fromData($this->getData() ?? []);
  }

  protected function getData(): ?array {
    $cid = 'h4c_bnn_gis';
    $current = \Drupal::time()->getRequestTime();
    $expiration = $current + 3600 * 24 * 30;

    $cached = $this->cacheBackend->get($cid);
    if ($cached) {
      return $cached->data;
    }
    else {
      $data = $this->fetchData();
      if (!is_null($data)) {
        $this->cacheBackend->set($cid, $data, $expiration, ['h4c_bnn_gis']);
        return $data;
      }
      else {
        return NULL;
      }
    }
  }

  protected function fetchData(): ?array {
    return $this->fetchRemoteData()
      ?? $this->fetchLocalData();
  }

  /**
   * @internal
   */
  public function fetchRemoteData(): ?array {
    return $this->extractDataFromCsvData($this->csvDecode($this->fetchFromRemote('https://stadtplan.bonn.de/csv?OD=171', 'text/csv')));
  }

  /**
   * @internal
   */
  public function fetchLocalData(): ?array {
    return $this->extractDataFromJsonData($this->jsonDecode($this->fetchJsonFromModule()));
  }

  /**
   * @internal
   */
  public function extractDataFromJsonData(?array $rawData): ?array {
    if (!isset($rawData)) {
      return NULL;
    }
    assert(is_array($rawData));
    assert($rawData['type'] === 'FeatureCollection');

    $data = [];
    foreach ($rawData['features'] ?? [] as $feature) {
      assert(is_array($feature));
      assert($feature['type'] === 'Feature');
      $id = $feature['properties']['objekt']; // German spelling!
      $street = $feature['properties']['stassen_langname'];
      $number = $feature['properties']['hnr'];
      $data[$street][$number] = $id;
    }
    return $data;
  }

  /**
   * @internal
   */
  public function extractDataFromCsvData(?array $rawData): ?array {
    if (!isset($rawData)) {
      return NULL;
    }
    assert(is_array($rawData));

    $data = [];
    foreach ($rawData ?? [] as $row) {
      assert(is_array($row));
      $id = $row['objekt']; // German spelling!
      $street = $row['stassen_langname'];
      $number = $row['hnr'];
      $data[$street][$number] = $id;
    }
    return $data;
  }

  /**
   * @internal
   */
  public function csvDecode(?string $csv): ?array {
    if (!isset($csv)) {
      return NULL;
    }
    return iterator_to_array(
      Reader::createFromString($csv)
        ->setDelimiter(';')
        ->setHeaderOffset(0)
        ->getRecords()
    );
  }

  /**
   * @internal
   */
  public function jsonDecode(?string $json): ?array {
    if (!isset($json)) {
      return NULL;
    }
    $rawData = json_decode($json, TRUE);
    if (json_last_error()) {
      // @todo Log.
      return NULL;
    }
    return $rawData;
  }

  /**
   * @internal
   */
  public function fetchFromRemote(string $remoteUrl, string $expectedContentType = NULL): ?string {
    try {
      $client = \Drupal::httpClient();
      $response = $client->get($remoteUrl);
      if ($response->getStatusCode() !== 200) {
        return NULL;
      }
      $originalBody = $response->getBody()->getContents();
      $contentTypeHeader = $response->getHeader('content-type');
      $contentTypeParsed = Header::parse($contentTypeHeader);
      $contentType = $contentTypeParsed[0][0] ?? NULL;
      $originalEncoding = $contentTypeParsed[0]['charset'] ?? NULL;
      $body = !$originalEncoding ? $originalBody :
        (mb_convert_encoding($originalBody, 'UTF-8', $originalEncoding) ?: NULL);
    } catch (GuzzleException $e) {
      // @todo Log.
      return NULL;
    }
    if ($contentType !== $expectedContentType) {
      return NULL;
    }
    return $body;
  }

  /**
   * @internal
   */
  public function fetchJsonFromModule(): string {
    return file_get_contents(dirname(__DIR__) . '/data/geojson.json');
  }

}
