<?php

namespace Drupal\Tests\h4c_bnn_gis;

use Drupal\Core\Cache\MemoryBackend;
use Drupal\h4c_bnn_gis\BnnGisStreetData;
use Drupal\h4c_bnn_gis\BnnGisStreetDataFactory;
use Drupal\h4c_bnn_gis\BnnGisStreetDataMatcher;
use PHPUnit\Framework\TestCase;

class BnnGisStreetDataMatcherTest extends TestCase {

  protected $matcher;

  public function testMatching(): void {
    $streetDataFactory = new BnnGisStreetDataFactory(new MemoryBackend());
    $data = $streetDataFactory->fetchLocalData();
    $this->assertNotNull($data);

    $streetData = BnnGisStreetData::fromData($data);
    $this->assertSame(2091, count($streetData->getStreets()));
    $this->assertSame(63598, count($streetData->getStreetAndNumberMapByIdMap()));

    $this->matcher = new BnnGisStreetDataMatcher($streetData);

    $this->assertSame('#Schlo(?:ß|ss)str(?:a(?:ß|ss)e|[.])? *(?<number>[0-9]+[a-z]?(?:-[0-9]+)?)#ui', $this->matcher->getRegex('Schloßstraße'));
    $this->assertSame('#Am[ -]?B(?:ö|oe)selagerhof *(?<number>[0-9]+[a-z]?(?:-[0-9]+)?)#ui', $this->matcher->getRegex('Am Boeselagerhof'));

    $this->assertSame(NULL, $this->matcher->match('Schloßstraße 2'));
    $this->assertSame('1021-1', $this->matcher->match('Am Boeselagerhof 1, bonn'));
    $this->assertSame('1021-1', $this->matcher->match('Am-Boeselagerhof 1, bonn'));
    $this->assertSame('1021-1', $this->matcher->match('Am Böselagerhof 1, bonn'));
    $this->assertSame('1697-2', $this->matcher->match('Schloßstraße 2, bonn'));
    $this->assertSame('1697-2', $this->matcher->match('Schlossstrasse 2, bonn'));
    $this->assertSame('1697-2', $this->matcher->match('schlossstrasse 2, bonn-buxtehude'));
    $this->assertSame('1697-2', $this->matcher->match('schlossstr2, bonn'));
    $this->assertSame('1697-23-25', $this->matcher->match('Schloßstraße 23-25, bonn'));
    $this->assertSame('2138-15c', $this->matcher->match('bonn, Giselherstraße 15c'));
  }


}
